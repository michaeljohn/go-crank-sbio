# SBIO

## Storyboard IO library for Go

This library is a wrapper to the Storyboard IO interface.

### Testing

    mk test

 Will run the Go tests on the library. This will test Go-to-Go usage of the library.

    cd tests
    mk all

 Will run the Go-to-C and C-to-Go tests.

 Between these two sets of tests there should be some example of what you wish to do.

## Gotchas

- When sending event data from either C-to-Go or Go-to-C, a structure cannot be passed in the `data` field for the `serialized()` call.
- Arrays, Slices, and Maps cannot be used in Go. This is for both Go-to-C and Go-to-Go.


## Notes

- Testing was done on 64bit Linux.
- Testing for Go used the Go 1.6 compiler
- Testing for C used the Clang 3.6.2 compiler
- Testing used Storyboard 4.1
- The library expects to be found at `$GOPATH/src/crown/sbio`.

The C compiler and Storyboard versions can be changed in the `mkconfig` file.

## Building a Go application using SBIO

To build this simple application for our ARM target:
    
    package main
    
    import (
	    "crown/sbio"
	    "fmt"
    )

    func main() {
	    var g sbio.GreIO
	    ret := g.Open("chan_sbio", sbio.IO_RDONLY)
	    if ret != nil {
		    fmt.Printf("%v\n", ret)
	    }

    }

We would use the following command:
    
    CC=armv7l-timesys-linux-gnueabi-gcc CGO_ENABLED=1 GOARCH=arm GOARM=7 \
    CGO_CFLAGS="-I ${CRANK_PATH}/include" \
    CGO_LDFLAGS="-L ${CRANK_PATH}/lib ${CRANK_PATH}/lib/libgreio.a -lrt" \
    go build -v -x -work main.go


