// Project:		C1515
// Copyright:		2015-2016 Crown Equipment Corp., New Bremen, OH 45869
// Author:		Michael John

// Package sbio is a Go library for the Crank Storyboard IO library. This library
// provides a wrapper to the C interface. The interface has been
// simplified some but is still similar to the original.
//
// This has been tested with Storyboard 4.1
//
package sbio

import (
	// built-ins

	"bytes"
	"encoding/binary"
	"encoding/gob"
	"fmt"
	"reflect"
	"strings"
	"unsafe"
)

// #cgo LDFLAGS: -lgreio -lrt
// #include "gre/greio.h"
// #include <stdlib.h>
// gre_io_t* Greioopen(const char *io_name, int flag) {
//     return gre_io_open(io_name, flag);
// }
import "C"

// GreIO is the main type for working with StoryboardIO.
type GreIO struct {
	Buffer *C.gre_io_serialized_data_t
	Handle *C.gre_io_t
}

// AddMdata adds a data change key/value pair to a serialized buffer.
// This call can be used to serialize multiple data changes into a single
// Storyboard IO send operation to improve efficiency.
//
// Once an multi-part data buffer is constructed, it can be sent using the
// gre_io_send_mdata function.
//
// Parameters:
// 	key		The data key which is to be set
//	dataFormat	The format for the data to be set
//	data		The data value to set
//
// Returns:
//	Error on failure, nil on success
//
func (g *GreIO) AddMdata(key, dataFormat string, data ...interface{}) error {
	_key := C.CString(key)
	defer C.free(unsafe.Pointer(_key))
	_dformat := C.CString(dataFormat)
	defer C.free(unsafe.Pointer(_dformat))

	buf := new(bytes.Buffer)
	var err error
	// Walk through data, determine what the type is for each
	for _, d := range data {
		val := reflect.ValueOf(d)
		switch val.Kind() {
		case reflect.Int8:
			fallthrough
		case reflect.Int16:
			fallthrough
		case reflect.Int32:
			fallthrough
		case reflect.Int64:
			fallthrough
		case reflect.Uint8:
			fallthrough
		case reflect.Uint16:
			fallthrough
		case reflect.Uint32:
			fallthrough
		case reflect.Uint64:
			fallthrough
		case reflect.Float32:
			fallthrough
		case reflect.Float64:
			if err = binary.Write(buf, binary.LittleEndian, d); err != nil {
				fmt.Printf("%v\n", err.Error())
			}
		case reflect.Struct:
			enc := gob.NewEncoder(buf)
			if err = enc.Encode(d); err != nil {
				fmt.Printf("%v\n", err.Error())
			}
		case reflect.String:
			if err = binary.Write(buf, binary.LittleEndian, []byte(d.(string))); err != nil {
				fmt.Printf("%v\n", err.Error())
			}
		}
	}

	// Copy data into a C buffer, so there is no pointer in Go memory. This is nessasary to avoid an error in cgo.
	str := buf.String()
	_data := unsafe.Pointer(C.CString(str))
	defer C.free(_data)

	if ret, err := C.gre_io_add_mdata(&g.Buffer, _key, _dformat, _data, C.int(buf.Len())); ret == -1 {
		// Error
		return fmt.Errorf("Failed: gre_io_add_mdata(): %v", err)
	}
	return nil
}

// Close an io connection.   Any pending clients will return with an error
// on their pending actions.
//
func (g *GreIO) Close() {
	C.gre_io_close(g.Handle)
	g.Handle = nil
}

// FreeBuffer de-allocates the memory associated with a buffer created through
// the Storyboard IO API.
//
func (g *GreIO) FreeBuffer() {
	C.gre_io_free_buffer(g.Buffer)
	g.Buffer = nil
}

// GrowBuffer attempts to expand the internal capacity of the Storyboard IO
// transport to ensure that the payload contained within serialized buffer can
// be transmitted.
//
// Note:
//	This call is not supported by all platforms and may fail if the
//	transport buffer can not be resized.
//
// Returns:
// 	Error on failure, nil on success
//
func (g *GreIO) GrowBuffer() error {
	if ret, err := C.gre_io_grow_buffer(g.Handle, g.Buffer); ret == -1 {
		return fmt.Errorf("Failed: gre_io_grow_buffer(): %v", err)
	}
	return nil
}

// Open Flags
const (
	IO_RDONLY        int = C.GRE_IO_TYPE_RDONLY
	IO_XRDONLY       int = C.GRE_IO_TYPE_XRDONLY
	IO_WRONLY        int = C.GRE_IO_TYPE_WRONLY
	IO_FLAG_NONBLOCK int = C.GRE_IO_FLAG_NONBLOCK
)

// Open a Storyboard IO communication channel using a named connection.
//
// Parameters:
// 	name	The name of the io-channel to use
// 	flags	The mode you want to open the queue in
//
//	Flags define how the connection is opened.  Possible flags are:
//		IO_RDONLY:     open read only, creating the channel if it doesn't exist
//		IO_XRDONLY:    open for exclusive read, unlinking an existing channel and creating a new one
//		IO_WRONLY:     open write only
//		IO_FLAG_NONBLOCK:   open non-blocking
//
// Returns:
//	Error if no channel could be created, nil on success
//
func (g *GreIO) Open(name string, flag int) error {
	_name := C.CString(name)
	defer C.free(unsafe.Pointer(_name))
	var err error
	g.Handle, err = C.Greioopen(_name, C.int(flag))
	if err != nil {
		return fmt.Errorf("Failed: gre_io_open(): %v", err)
	}
	return nil
}

// Receive a serialized event from a channel.  By default this call blocks until
// an event is received or until the channel is destroyed unless the
// IO_FLAG_NONBLOCK flag was passed to the Open() call.
//
// In order to receive events, the handle must have been opened for reading using
// one of IO_RDONLY or IO_XRDONLY.
//
// Parameters:
//	none
//
// Returns:
//	int:    -1 on failure, the size of the message received in bytes on success
//	error:  Error on failure, nil on success
//
func (g *GreIO) Receive() (int, error) {
	cret, err := C.gre_io_receive(g.Handle, &g.Buffer)
	ret := int(cret)
	if ret == -1 {
		return -1, fmt.Errorf("Failed: gre_io_receive(): %v", err)
	}
	return ret, nil
}

// Send a serialized event buffer to a channel. In order to send events, the
// handle must have been opened for writing using IO_WRONLY.
//
// Parameters:
//	none
//
// Returns:
//	Error on failure, nil on success.
//
func (g *GreIO) Send() error {
	if ret, err := C.gre_io_send(g.Handle, g.Buffer); ret == -1 {
		return fmt.Errorf("Failed: gre_io_send(): %v", err)
	}
	return nil
}

// SendMdata sends a serialized buffer of mdata (data manager key/value pairs) to the
// handle. The handle must have been opened for writing using IO_WRONLY.
//
// Parameters:
//	none
//
// Returns:
//	Error on failure, nil on success
//
func (g *GreIO) SendMdata() error {
	if ret, err := C.gre_io_send_mdata(g.Handle, g.Buffer); ret == -1 {
		return fmt.Errorf("Failed: gre_io_send_mdata(): %v", err)
	}
	return nil
}

// Serialize individual event items (see gre/io_mgr.h) into a single buffer
// for transmission using Storyboard IO.
//
// Parameters:
//	addr      The name of the event target, or nil to send to the default target
//	name      The name of the event to send, or nil to send an empty event
//	format    The format description of the data, or nil if no data is being sent
//	data      The data to transmit, or nil if no data is transmitted
//
// Returns:
//	Error on failure, nil on success
//
func (g *GreIO) Serialize(addr, name, format interface{}, data ...interface{}) error {
	var (
		err     error
		_addr   *C.char
		_name   *C.char
		_format *C.char
		_data   unsafe.Pointer
	)
	if addr != nil {
		_addr = C.CString(addr.(string))
		defer C.free(unsafe.Pointer(_addr))
	}
	if name != nil {
		_name = C.CString(name.(string))
		defer C.free(unsafe.Pointer(_name))
	}
	if format != nil {
		_format = C.CString(format.(string))
		defer C.free(unsafe.Pointer(_format))
	} else if len(data) > 1 { //We check greater than 1 and not 0 here b/c <nil> returns length = 1
		// ERROR, format can not be nil and have data
		return fmt.Errorf("Format cannot be nil and have data")
	}

	buf := new(bytes.Buffer)
	if format != nil {
		// Walk through data, determine what the type is for each
		for _, d := range data {
			val := reflect.ValueOf(d)
			switch val.Kind() {
			case reflect.Int8:
				fallthrough
			case reflect.Int16:
				fallthrough
			case reflect.Int32:
				fallthrough
			case reflect.Int64:
				fallthrough
			case reflect.Uint8:
				fallthrough
			case reflect.Uint16:
				fallthrough
			case reflect.Uint32:
				fallthrough
			case reflect.Uint64:
				fallthrough
			case reflect.Float32:
				fallthrough
			case reflect.Float64:
				if err = binary.Write(buf, binary.LittleEndian, d); err != nil {
					fmt.Printf("%v\n", err.Error())
				}
			case reflect.Struct:
				enc := gob.NewEncoder(buf)
				if err = enc.Encode(d); err != nil {
					fmt.Printf("%v\n", err.Error())
				}
			case reflect.String:
				encodedString := []byte(d.(string))
				// Must null terminate variable length strings being passed into greio
				if strings.Contains(format.(string), "1s0") {
					encodedString = []byte(d.(string) + "\x00")
				}
				if err = binary.Write(buf, binary.LittleEndian, encodedString); err != nil {
					fmt.Printf("%v\n", err.Error())
				}
			}
		}

		// Copy data into a C buffer, so there is no pointer in Go memory. This is nessasary to avoid an error in cgo.
		str := buf.String()
		_data = unsafe.Pointer(C.CString(str))
		defer C.free(_data)
	}

	// Call serialize function
	g.Buffer, err = C.gre_io_serialize(g.Buffer, _addr, _name, _format, _data, C.int(buf.Len()))
	if g.Buffer == nil || err != nil {
		return fmt.Errorf("Failed: gre_io_serialize(): %v", err)
	}
	return nil
}

// SizeBuffer ensures that the specified buffer has enough internal storage
// capacity for a payload of nbytes size. If the buffer is nil or the existing
// capacity is not large enough then a new memory buffer will be assigned to the
// buffer object.
//
// Parameters:
//	nbytes	The number of bytes this buffer should be able to support
//
// Returns:
//	Error on failure if the space could not be allocated, nil on success
//
func (g *GreIO) SizeBuffer(nbytes int) error {
	ret, err := C.gre_io_size_buffer(g.Buffer, C.int(nbytes))
	if ret == nil {
		return fmt.Errorf("Failed: space could not be allocated: %v", err)
	}
	C.free(unsafe.Pointer(g.Buffer))
	g.Buffer = ret
	return nil
}

// Unserialize transforms a serialized buffer into individual event items (see gre/io_mgr.h).
//
// Parameters:
//	none
//
// Returns:
//	addr:    event target
//	name:    event name
//	format:  event format
//	data:    event data
//
func (g *GreIO) Unserialize() (a interface{}, n interface{}, f interface{}, d []byte) {
	// Create C buffers for return data
	var addr string
	var name string
	var format string
	var data string
	_addr := C.CString(addr)
	defer C.free(unsafe.Pointer(_addr))
	_name := C.CString(name)
	defer C.free(unsafe.Pointer(_name))
	_format := C.CString(format)
	defer C.free(unsafe.Pointer(_format))
	_data := unsafe.Pointer(C.CString(data))
	defer C.free(_data)

	nbytes := C.gre_io_unserialize(g.Buffer, &_addr, &_name, &_format, &_data)

	// Convert back to Go types before returning
	if *_addr == 0 {
		a = nil
	} else {
		a = C.GoString(_addr)
	}
	if *_name == 0 {
		n = nil
	} else {
		n = C.GoString(_name)
	}
	if *_format == 0 {
		f = nil
		d = []byte{}
	} else {
		f = C.GoString(_format)
		d = C.GoBytes(_data, nbytes)
	}
	return
}

// ZeroBuffer clears the internal byte count of the buffer, but does not de-allocate
// the buffer's memory.
//
// Use this function to reset a buffer in between multiple calls to Serialize()
//
func (g *GreIO) ZeroBuffer() {
	C.gre_io_zero_buffer(g.Buffer)
}
