package sbio

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"math"
	"testing"
	"time"
)

func Test_libsbio_GreIO(t *testing.T) {
	var g GreIO
	if nil != g.Buffer {
		t.Fatal("GreIO Buffer variable is not initialized as nil")
	}
	if nil != g.Handle {
		t.Fatal("GreIO Handle variable is not initialized as nil")
	}
}

func Test_libsbio_Open_Close(t *testing.T) {
	var g GreIO
	// Open a SBIO communication channel
	ret := g.Open("chan_sbio", IO_RDONLY)
	if ret != nil {
		t.Fatalf("%v\n", ret)
	}
	g.Close()
	if nil != g.Handle {
		t.Fatal("GreIO Handle variable is not initialized as nil")
	}
}

func Test_libsbio_Buffer(t *testing.T) {
	var g GreIO
	ret := g.SizeBuffer(10)
	if ret != nil {
		t.Fatalf("%v\n", ret.Error())
	}
	if g.Buffer == nil {
		t.Fatal("Buffer is still nil")
	}
	g.FreeBuffer()
	if g.Buffer != nil {
		t.Fatal("Buffer should be nil")
	}
	ret = g.SizeBuffer(100)
	if ret != nil {
		t.Fatalf("%v\n", ret.Error())
	}
}

func Test_libsbio_Mdata(t *testing.T) {
	var g GreIO
	ret := g.AddMdata("Key", "1s0", "Data sent via Mdata call")
	if ret != nil {
		t.Fatalf("AddMdata call with a string: %v\n", ret.Error())
	}
	var a int8 = -3
	var b int16 = 126
	var c uint32 = 100000
	var d uint64 = 1
	ret = g.AddMdata("Blah", "1s1 2s1 4u1 8u1", a, b, c, d)
	if ret != nil {
		t.Fatalf("AddMdata call with ints: %v\n", ret.Error())
	}
}

func Test_libsbio_serial_unserial(t *testing.T) {
	var g GreIO
	ret := g.Serialize("Test", "Press", "1s0", "The press data")
	if ret != nil {
		t.Fatalf("%v\n", ret.Error())
	}

	a, n, f, d := g.Unserialize()
	data := string(d[:])
	t.Logf("UN: %v %v %v %v %v\n", a, n, f, data, len(data))
	if a != "Test" {
		t.Fatal("addr did not match after unserialize()")
	}
	if n != "Press" {
		t.Fatal("name did not match after unserialize()")
	}
	if f != "1s0" {
		t.Fatal("format did not match after unserialize()")
	}
	if data != "The press data" {
		t.Fatal("data did not match after unserialize()")
	}
}

func getData(store interface{}, data []byte, start, len int) {
	by := bytes.NewBuffer(data[start : start+len])
	binary.Read(by, binary.LittleEndian, store)
}

func Test_libsbio_serial_unserial_multi(t *testing.T) {
	var (
		g    GreIO
		vala int8   = -3
		valb int16  = 16
		valc uint8  = 1
		vald string = "blah foo bar"
	)

	ret := g.Serialize("Test", "Press", "1s1 1s12 2s1 1u1", vala, vald, valb, valc)
	if ret != nil {
		t.Fatalf("%v\n", ret.Error())
	}

	_, _, _, d := g.Unserialize()
	reta := int8(d[0])
	var retb int16
	getData(&retb, d, 13, 2)
	var retc uint8
	getData(&retc, d, 15, 1)
	retd := string(d[1:13])
	t.Logf("UN: %v %v %v %v\n", reta, retb, retc, retd)
	if reta != vala {
		t.Fatal("a did not match")
	}
	if retb != valb {
		t.Fatal("b did not match")
	}
	if retc != valc {
		t.Fatal("c did not match")
	}
	if retd != vald {
		t.Fatal("d did not match")
	}
}

func Test_libsbio_send_receive(t *testing.T) {
	var recv GreIO
	err := recv.Open("sbio_receive", IO_RDONLY)
	if err != nil {
		t.Fatalf("%v\n", err)
	}
	var send GreIO
	err = send.Open("sbio_receive", IO_WRONLY)
	if err != nil {
		t.Fatalf("%v\n", err)
	}
	// Receive portion
	go func() {
		recv.FreeBuffer()
		retlen, err := recv.Receive()
		t.Logf("Receive return: %v\n", retlen)
		if err != nil {
			t.Fatalf("%v\n", err.Error())
		}
		a, n, f, d := recv.Unserialize()
		data := string(d[:])
		t.Logf("rets: %v %v %v %v %v\n", a, n, f, data, len(data))
		recv.Close()
	}()
	// Send portion
	send.Serialize("Test", "Press", "1s0", "Data to send in libsbio_test")
	err = send.Send()
	if err != nil {
		t.Fatalf("%v\n", err.Error())
	}
	send.Close()
	// Pause to let the receive thread finish
	time.Sleep(50 * time.Millisecond)
}

func Test_libsbio_multi_send(t *testing.T) {
	var recv GreIO
	if err := recv.Open("sbio_receive", IO_RDONLY); err != nil {
		t.Fatalf("%v\n", err)
	}
	defer recv.Close()
	var send GreIO
	if err := send.Open("sbio_receive", IO_WRONLY); err != nil {
		t.Fatalf("%v\n", err)
	}
	defer send.Close()

	// Send messages
	go func() {
		if err := send.Serialize("Test", "Press", "1s0", "Data to send 1"); err != nil {
			t.Fatalf("%v\n", err.Error())
		}
		if err := send.Send(); err != nil {
			t.Fatalf("%v\n", err.Error())
		}
		if err := send.Serialize("Test", "Press", "1s0", "Data to send 2"); err != nil {
			t.Fatalf("%v\n", err.Error())
		}
		if err := send.Send(); err != nil {
			t.Fatalf("%v\n", err.Error())
		}
	}()

	// Recv messages
	recv.FreeBuffer()
	retlen, err := recv.Receive()
	t.Logf("Received: %v\n", retlen)
	if err != nil {
		t.Fatalf("%v\n", err.Error())
	}
	a, n, f, d := recv.Unserialize()
	data := string(d[:])
	t.Logf("rets: %v %v %v %v %v\n", a, n, f, data, len(data))
	recv.FreeBuffer()
	retlen, err = recv.Receive()
	t.Logf("Received: %v\n", retlen)
	if err != nil {
		t.Fatalf("%v\n", err.Error())
	}
	a, n, f, d = recv.Unserialize()
	data = string(d[:])
	t.Logf("rets: %v %v %v %v %v\n", a, n, f, data, len(data))
}

func Test_libsbio_serial_int8(t *testing.T) {
	var (
		vals  = []int8{8, 4, 20, -4, 0, -1, 1, -111, 127}
		g     GreIO
		recvd int8 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "1s1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		recvd = int8(d[0])
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_int16(t *testing.T) {
	var (
		vals  = []int16{8, 4, 2000, -4, 0, -1, 1, -1111}
		g     GreIO
		recvd int16 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "2s1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		by := bytes.NewBuffer(d[0:2])
		binary.Read(by, binary.LittleEndian, &recvd)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_int32(t *testing.T) {
	var (
		vals  = []int32{8, 4, 2000, -4, 0, -1, 1, -1111}
		g     GreIO
		recvd int32 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "4s1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		by := bytes.NewBuffer(d[0:4])
		binary.Read(by, binary.LittleEndian, &recvd)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_int64(t *testing.T) {
	var (
		vals  = []int64{8, 4, 2000, -4, 0, -1, 1, -1111}
		g     GreIO
		recvd int64 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "8s1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		by := bytes.NewBuffer(d[0:8])
		binary.Read(by, binary.LittleEndian, &recvd)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_uint8(t *testing.T) {
	var (
		vals  = []uint8{8, 4, 200, 0, 1, 111}
		g     GreIO
		recvd uint8 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "1u1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		recvd = uint8(d[0])
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_uint16(t *testing.T) {
	var (
		vals  = []uint16{8, 4, 2000, 0, 1, 1111}
		g     GreIO
		recvd uint16 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "2u1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		by := bytes.NewBuffer(d[0:2])
		binary.Read(by, binary.LittleEndian, &recvd)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_uint32(t *testing.T) {
	var (
		vals  = []uint32{8, 4, 2000, 0, 1, 1111}
		g     GreIO
		recvd uint32 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "4u1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		by := bytes.NewBuffer(d[0:4])
		binary.Read(by, binary.LittleEndian, &recvd)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_uint64(t *testing.T) {
	var (
		vals  = []uint64{8, 4, 2000, 0, 1, 1111}
		g     GreIO
		recvd uint64 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "8u1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		by := bytes.NewBuffer(d[0:8])
		binary.Read(by, binary.LittleEndian, &recvd)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_float32(t *testing.T) {
	var (
		vals  = []float32{0, 1, -1, 1.0, -1.0, 0.1, -0.1, 100.0, 15253.53536}
		g     GreIO
		recvd float32 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "4f1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		bits := binary.LittleEndian.Uint32(d)
		recvd = math.Float32frombits(bits)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_float64(t *testing.T) {
	var (
		vals  = []float64{0, 1, -1, 1.0, -1.0, 0.1, -0.1, 100.0, 15253.53536}
		g     GreIO
		recvd float64 //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "8f1", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		bits := binary.LittleEndian.Uint64(d)
		recvd = math.Float64frombits(bits)
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_string(t *testing.T) {
	var (
		vals  = []string{"test", "", " ", " done with test?"}
		g     GreIO
		recvd string //Receive
	)

	for _, val := range vals {
		ret := g.Serialize("Test", "Press", "1s0", val)
		if ret != nil {
			t.Fatalf("%v\n", ret.Error())
		}
		_, _, _, d := g.Unserialize()
		recvd = string(d[:])
		if recvd != val {
			t.Fatal("data did not match after unserialize()")
		}
	}
}

func Test_libsbio_serial_nil(t *testing.T) {
	var (
		g GreIO
	)

	ret := g.Serialize("Test", "Press", nil, nil)
	if ret != nil {
		t.Fatalf("%v\n", ret.Error())
	}
	a, n, f, _ := g.Unserialize()
	if f != nil {
		fmt.Printf("rets: %v %v %v\n", a, n, f)
		t.Fatal("data did not match after unserialize()")
	}

	ret = g.Serialize("Test", nil, nil, nil)
	if ret != nil {
		t.Fatalf("%v\n", ret.Error())
	}
	a, n, f, _ = g.Unserialize()
	if n != nil {
		fmt.Printf("rets: %v %v |%v|\n", a, n, f)
		t.Fatal("data did not match after unserialize()")
	}

	ret = g.Serialize(nil, nil, nil, nil)
	if ret != nil {
		t.Fatalf("%v\n", ret.Error())
	}
	a, n, f, _ = g.Unserialize()
	if a != nil {
		fmt.Printf("rets: %v %v |%v|\n", a, n, f)
		t.Fatal("data did not match after unserialize()")
	}
}
