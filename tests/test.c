#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gre/greio.h"

#define FAILURE 1
#define SUCCESS 0
#define CHANNEL "example"

int test1(void);
int test2(void);
int test3(void);
int test4(void);
int test5(void);

int
main(const int argc, const char **argv) {
        if (argc != 2) {
                printf("Usage: %s <#>\n", argv[0]);
                return FAILURE;
        }
        int test = atoi(argv[1]);
        switch (test) {
                case 1: return test1();
                case 2: return test2();
                case 3: return test3();
                case 4: return test4();
                case 5: return test5();
        }
        return FAILURE;
}

gre_io_t*
openSend(void) {
        gre_io_t *h;
        h = gre_io_open(CHANNEL, GRE_IO_TYPE_WRONLY);
        if (NULL == h) {
                printf("Can't open send handle\n");
                return NULL;
        }
        return h;
}

gre_io_t*
openRecv(void) {
        gre_io_t *h;
        h = gre_io_open(CHANNEL, GRE_IO_TYPE_RDONLY);
        if (NULL == h) {
                printf("Can't open receive handle\n");
                return NULL;
        }
        return h;
}

int
test1(void) {
        gre_io_t *send = openSend();
        if (NULL == send) {
                return FAILURE;
        }

        gre_io_serialized_data_t *nbuffer = NULL;
        int i = 42; //value to send
        nbuffer = gre_io_serialize(nbuffer, NULL, "bar", "4s1", &i, 4);
        if (NULL == nbuffer) {
                return FAILURE;
        }
        if (-1 == gre_io_send(send, nbuffer)) {
                return FAILURE;
        }
        gre_io_close(send);
        return SUCCESS;
}

int
test2(void) {
        gre_io_t *recv = openRecv();
        if (NULL == recv) {
                return FAILURE;
        }

        gre_io_serialized_data_t *nbuffer = NULL;
        if (-1 == gre_io_receive(recv, &nbuffer)) {
                return FAILURE;
        }

        int rnbytes;
        char *name;
        char *target;
        char *format;
        uint8_t *data;
        rnbytes = gre_io_unserialize(nbuffer, &target, &name, &format, (void **)&data);
        int32_t *i = (int32_t*)data;
        //check if successfully received and unserialized value
        int32_t expected = 1999555;
        if (expected != *i) {
                printf("Event: %s\nTarget: %s\nFormat: %s\nData: %d\nRet: %d\n", name, target, format, *i, rnbytes);
                return FAILURE;
        }
        gre_io_close(recv);
        return SUCCESS;
}

int
test3(void) {
        gre_io_t *send = openSend();
        if (NULL == send) {
                return FAILURE;
        }

        gre_io_serialized_data_t *nbuffer = NULL;
        nbuffer = gre_io_serialize(nbuffer, NULL,
                 "bar", "1s0", "This is the message", 19);
        if (NULL == nbuffer) {
                return FAILURE;
        }
        if (-1 == gre_io_send(send, nbuffer)) {
                return FAILURE;
        }
        gre_io_close(send);
        return SUCCESS;
}

int
test4(void) {
        gre_io_t *recv = openRecv();
        if (NULL == recv) {
                return FAILURE;
        }

        gre_io_serialized_data_t *nbuffer = NULL;
        if (-1 == gre_io_receive(recv, &nbuffer)) {
                return FAILURE;
        }

        int rnbytes;
        char *name;
        char *target;
        char *format;
        uint8_t *data;
        rnbytes = gre_io_unserialize(nbuffer, &target, &name, &format, (void **)&data);
        char *i = (char*)data;
        i[rnbytes] = '\0';
        if (0 != strncmp(i, "This is the test message", rnbytes)) {
                printf("Event: %s\nTarget: %s\nFormat: %s\nData: %s\nRet: %d\n", name, target, format, i, rnbytes);
                return FAILURE;
        }
        gre_io_close(recv);
        return SUCCESS;
}

int
test5(void) {
        gre_io_t *recv = openRecv();
        if (NULL == recv) {
                return FAILURE;
        }

        gre_io_serialized_data_t *nbuffer = NULL;
        if (-1 == gre_io_receive(recv, &nbuffer)) {
                return FAILURE;
        }

        int rnbytes;
        char *name;
        char *target;
        char *format;
        uint8_t *data;
        rnbytes = gre_io_unserialize(nbuffer, &target, &name, &format, (void **)&data);
        int32_t *a = (int32_t*)data;
        uint8_t *b = (uint8_t*)(data+4);
        int16_t *c = (int16_t*)(data+5);
        int8_t *d = (int8_t*)(data+7);
        //check if successfully received and unserialized value
        int32_t ae = 1999555;
        uint8_t be = (uint8_t)7;
        int16_t ce = (int16_t)-200;
        int8_t de = (int8_t)-3;
        if (ae != *a || be != *b || ce != *c || de != *d) {
                printf("Event: %s\nTarget: %s\nFormat: %s\nData: %d\nRet: %d\n", name, target, format, *a, rnbytes);
                return FAILURE;
        }
        //printf("%d %u %d %d\n", *a, *b, *c, *d);
        gre_io_close(recv);
        return SUCCESS;
}
