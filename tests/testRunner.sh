#!/bin/sh

if [ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ]; then
        echo "USAGE: $0 <#> <test> <test>"
        exit 1
fi

echo "=== RUN   Test $1"
./$2 $1 &
pid=$!
sleep 1
./$3 $1
rc2=$?
wait $pid
rc1=$?
if [ $rc1 -eq 0 ] && [ $rc2 -eq 0 ]; then
        echo "--- PASS: Test $1"
else
        echo "--- FAIL: Test $1"
fi
