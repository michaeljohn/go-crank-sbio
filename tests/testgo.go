package main

import (
	"bytes"
	"crown/sbio"
	"encoding/binary"
	"fmt"
	"os"
)

func main() {
	var rc = 0
	switch os.Args[1] {
	case "1":
		rc = test1()
	case "2":
		rc = test2()
	case "3":
		rc = test3()
	case "4":
		rc = test4()
	case "5":
		rc = test5()
	}
	os.Exit(rc)
}

func test1() int {
	var recv sbio.GreIO
	recv.Open("example", sbio.IO_RDONLY)
	defer recv.Close()

	ret, err := recv.Receive()
	if ret == -1 || err != nil {
		fmt.Printf("Failed on receive: %v\n", err.Error())
		return 1
	}

	a, n, f, d := recv.Unserialize()
	var val int32
	by := bytes.NewBuffer(d[0:4])
	binary.Read(by, binary.LittleEndian, &val)
	if val != 42 {
		fmt.Println("Did not get expected value")
		fmt.Printf("%v %v %v %v\n", a, n, f, val)
		return 1
	}
	return 0
}

func test2() int {
	var send sbio.GreIO
	send.Open("example", sbio.IO_WRONLY)
	defer send.Close()

	var in3 int32
	in3 = 1999555
	send.Serialize("Test", "Press", "4s1", in3)
	send.Send()
	return 0
}

func test3() int {
	var recv sbio.GreIO
	recv.Open("example", sbio.IO_RDONLY)
	defer recv.Close()

	ret, err := recv.Receive()
	if ret == -1 || err != nil {
		fmt.Printf("Failed on receive: %v\n", err.Error())
		return 1
	}

	a, n, f, d := recv.Unserialize()
	if string(d) != "This is the message" {
		fmt.Println("Did not get expected value")
		fmt.Printf("%v %v %v %v\n", a, n, f, string(d))
		return 1
	}
	return 0
}

func test4() int {
	var send sbio.GreIO
	send.Open("example", sbio.IO_WRONLY)
	defer send.Close()

	send.Serialize("Test", "Press", "1s0", "This is the test message")
	send.Send()
	return 0
}

func test5() int {
	var send sbio.GreIO
	send.Open("example", sbio.IO_WRONLY)
	defer send.Close()

	var (
		a int32 = 1999555
		b uint8 = 7
		c int16 = -200
		d int8  = -3
	)
	send.Serialize("Test", "Press", "4s1 1u1 2s1 1s1", a, b, c, d)
	send.Send()
	return 0
}
